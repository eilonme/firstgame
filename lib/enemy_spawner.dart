import 'package:first_game/componets/enemy.dart';
import 'package:first_game/game_controller.dart';
import 'package:flutter/cupertino.dart';

class EnemySpawner {
  final GameController gameController;
  final int maxSpawnInterval = 3000;
  final int minSpawnInterval = 700;
  final int intervalChange = 3;
  final int maxEnemies = 3;
  int currentInterval;
  int nextSpawn;

  EnemySpawner(this.gameController) {
    killAllEnemies();
    initialize();
  }

  void initialize() {
    currentInterval = maxSpawnInterval;
    nextSpawn = DateTime.now().millisecondsSinceEpoch + currentInterval;
  }

  void killAllEnemies() {
    gameController.enemies.forEach((Enemy enemy) => enemy.isDead = true);
  }

  void update(double t) {
    int now = DateTime.now().millisecondsSinceEpoch;
    if (gameController.enemies.length <= maxEnemies && now > nextSpawn) {
      gameController.spawnEnemies();
      if (currentInterval > minSpawnInterval) {
        currentInterval -= intervalChange;
        currentInterval -= (currentInterval * 0.1).toInt();
      }

      nextSpawn = now + currentInterval;
    }
  }
}
