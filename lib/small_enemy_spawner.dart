import 'dart:math';

import 'package:first_game/componets/small_enemy.dart';
import 'package:first_game/game_controller.dart';
import 'package:flutter/cupertino.dart';

class SmallEnemySpawner {
  final GameController gameController;
  final int initialSpawnInterval = 30000;
  final int maxSpawnInterval = 3000;
  final int minSpawnInterval = 700;
  final int intervalChange = 3;
  final int maxSmallEnemies = 5;
  int currentInterval;
  int nextSpawn;
  bool needInGame = false;

  SmallEnemySpawner(this.gameController) {
    killAllEnemies();
    initialize();
  }

  void initialize() {
    currentInterval = maxSpawnInterval;
    nextSpawn = DateTime.now().millisecondsSinceEpoch + initialSpawnInterval;
  }

  void killAllEnemies() {
    gameController.smallEnemies
        .forEach((SmallEnemy smallEnemy) => smallEnemy.isDead = true);
  }

  void update(double t) {
    int now = DateTime.now().millisecondsSinceEpoch;
    if (gameController.smallEnemies.length <= maxSmallEnemies &&
        now > nextSpawn) {
      gameController.spawnSmallEnemies();
      if (currentInterval > minSpawnInterval) {
        currentInterval -= intervalChange;
        currentInterval -= (currentInterval * 0.1).toInt();
      }

      nextSpawn = now + currentInterval;
    }
  }
}
