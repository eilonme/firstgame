import 'dart:math';

import 'package:first_game/ally_spawner.dart';
import 'package:first_game/componets/ally.dart';
import 'package:first_game/componets/enemy.dart';
import 'package:first_game/componets/freeze_spell.dart';
import 'package:first_game/componets/health_bar.dart';
import 'package:first_game/componets/small_enemy.dart';
import 'package:first_game/enemy_spawner.dart';
import 'package:first_game/small_enemy_spawner.dart';
import 'package:flame/game.dart';
import 'package:flutter/cupertino.dart';
import 'package:flame/flame.dart';
import 'package:flutter/material.dart';
import 'package:first_game/componets/player.dart';

class GameController extends Game {
  Random rand;
  Size screenSize;
  double tileSize;
  Player player;
  FreezeSpell freezeSpell;
  EnemySpawner enemySpawner;
  SmallEnemySpawner smallEnemySpawner;
  AllySpawner allySpawner;
  List<Enemy> enemies;
  List<SmallEnemy> smallEnemies;
  List<Ally> allies;
  HealthBar healthBar;

  GameController() {
    initialize();
  }

  void initialize() async {
    resize(await Flame.util.initialDimensions());
    rand = Random();
    player = Player(this);
    smallEnemies = List<SmallEnemy>();
    enemies = List<Enemy>();
    allies = List<Ally>();
    smallEnemySpawner = SmallEnemySpawner(this);
    enemySpawner = EnemySpawner(this);
    allySpawner = AllySpawner(this);
    healthBar = HealthBar(this);
    freezeSpell = FreezeSpell(this, 5.5, 5.5);
    spawnEnemies();
    spawnSmallEnemies();
    spawnAllies();
  }

  void render(Canvas c) {
    Rect background = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
    Paint backgrpundPaint = Paint()..color = Colors.black;
    c.drawRect(background, backgrpundPaint);

    player.render(c);
    enemies.forEach((Enemy enemy) => enemy.render(c));
    smallEnemies.forEach((SmallEnemy smallEnemy) => smallEnemy.render(c));
    allies.forEach((Ally ally) => ally.render(c));
    healthBar.render(c);
    freezeSpell.render(c);
  }

  void update(double t) {
    enemySpawner.update(t);
    smallEnemySpawner.update(t);
    allySpawner.update(t);
    enemies.forEach((Enemy enemy) => enemy.update(t));
    enemies.removeWhere((Enemy enemy) => enemy.isDead);
    smallEnemies.forEach((SmallEnemy smallEnemy) => smallEnemy.update(t));
    smallEnemies.removeWhere((SmallEnemy smallEnemy) => smallEnemy.isDead);
    allies.forEach((Ally ally) => ally.update(t));
    allies.removeWhere((Ally ally) => ally.isDead);
    player.update(t);
    healthBar.upDate(t);
  }

  void resize(Size size) {
    screenSize = size;
    tileSize = screenSize.width / 10;
  }

  void onTapDown(TapDownDetails d) {
    print(d.globalPosition);
    enemies.forEach((Enemy enemy) {
      if (enemy.enemyRect.contains(d.globalPosition)) {
        enemy.onTapDown();
      }
    });
    smallEnemies.forEach((SmallEnemy smallEnemy) {
      if (smallEnemy.smallEnemyRect.contains(d.globalPosition)) {
        smallEnemy.onTapDown();
      }
    });
    allies.forEach((Ally ally) {
      if (ally.allyRect.contains(d.globalPosition)) {
        ally.onTapDown();
      }
    });
  }

  void spawnEnemies() {
    double x, y;
    switch (rand.nextInt(4)) {
      case 0:
        x = rand.nextDouble() * screenSize.width;
        y = -tileSize * 2.5;
        break;
      case 1:
        x = screenSize.width + tileSize * 2.5;
        y = rand.nextDouble() * screenSize.height;
        break;
      case 2:
        x = -tileSize * 2.5;
        y = rand.nextDouble() * screenSize.height;
        break;
      case 3:
        x = rand.nextDouble() * screenSize.width;
        y = tileSize * 2.5 + screenSize.height;
        break;
    }
    enemies.add(Enemy(this, x, y));
  }

  void spawnSmallEnemies() {
    double x, y;
    switch (rand.nextInt(4)) {
      case 0:
        x = rand.nextDouble() * screenSize.width;
        y = -tileSize * 2.5;
        break;
      case 1:
        x = screenSize.width + tileSize * 2.5;
        y = rand.nextDouble() * screenSize.height;
        break;
      case 2:
        x = -tileSize * 2.5;
        y = rand.nextDouble() * screenSize.height;
        break;
      case 3:
        x = rand.nextDouble() * screenSize.width;
        y = tileSize * 2.5 + screenSize.height;
        break;
    }
    smallEnemies.add(SmallEnemy(this, x, y));
  }

  void spawnAllies() {
    double x, y;
    switch (rand.nextInt(4)) {
      case 0:
        x = rand.nextDouble() * screenSize.width;
        y = -tileSize * 2.5;
        break;
      case 1:
        x = screenSize.width + tileSize * 2.5;
        y = rand.nextDouble() * screenSize.height;
        break;
      case 2:
        x = -tileSize * 2.5;
        y = rand.nextDouble() * screenSize.height;
        break;
      case 3:
        x = rand.nextDouble() * screenSize.width;
        y = tileSize * 2.5 + screenSize.height;
        break;
    }
    allies.add(Ally(this, x, y));
  }
}
