import 'package:first_game/game_controller.dart';
import 'package:flutter/cupertino.dart';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:first_game/main.dart';

class HealthBar {
  final GameController gameController;
  Rect healthBarRect;
  Rect remainingHealthBarRect;

  HealthBar(this.gameController) {
    double barWidth = gameController.screenSize.width / 1.75;
    healthBarRect = Rect.fromLTWH(
        gameController.screenSize.width / 2 - barWidth / 2,
        gameController.screenSize.height * 0.9,
        barWidth,
        gameController.tileSize * 0.3);
    remainingHealthBarRect = Rect.fromLTWH(
        gameController.screenSize.width / 2 - barWidth / 2,
        gameController.screenSize.height * 0.9,
        barWidth,
        gameController.tileSize * 0.3);
  }

  void render(Canvas c) {
    Paint healthBarColor = Paint()..color = Colors.red[800];
    Paint remainingHealthBarColor = Paint()..color = Colors.green[800];

    c.drawRect(healthBarRect, healthBarColor);
    c.drawRect(remainingHealthBarRect, remainingHealthBarColor);
  }

  void upDate(double t) {
    double barWidth = gameController.screenSize.width / 1.75;
    double percentHealth =
        gameController.player.currentHealth / gameController.player.maxHealth;
    remainingHealthBarRect = Rect.fromLTWH(
        gameController.screenSize.width / 2 - barWidth / 2,
        gameController.screenSize.height * 0.9,
        barWidth * percentHealth,
        gameController.tileSize * 0.3);
  }
}
