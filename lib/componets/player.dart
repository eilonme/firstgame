import 'dart:core';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:first_game/game_controller.dart';

class Player {
  final GameController gameController;
  int maxHealth;
  int currentHealth;
  Rect playerRect;
  bool isDead = false;

  Player(this.gameController) {
    maxHealth = currentHealth = 2500;
    final size = gameController.tileSize * 1.5;
    playerRect = Rect.fromLTWH((gameController.screenSize.width - size) / 2,
        (gameController.screenSize.height - size) / 2, size, size);
  }

  void render(Canvas c) {
    Color color;
    if (currentHealth <= 2500 && currentHealth > 1250) {
      color = Colors.green[800];
    } else if (currentHealth <= 1250 && currentHealth > 250) {
      color = Colors.green[600];
    } else if (currentHealth <= 250 && currentHealth > 0) {
      color = Colors.greenAccent[400];
    }
    if (isDead) {
      color = Colors.black;
    }

    Paint playerColor = Paint()..color = color;
    c.drawRect(playerRect, playerColor);
  }

  void update(double t) {
    if (!isDead && currentHealth <= 0) {
      isDead = true;
    }
  }
}
