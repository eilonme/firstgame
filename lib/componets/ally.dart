import 'package:first_game/game_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Ally {
  GameController gameController;
  int health;
  int support;
  double speed;
  Rect allyRect;
  bool isDead = false;

  Ally(this.gameController, x, y) {
    health = 1;
    support = 250;
    speed = gameController.tileSize;
    allyRect = Rect.fromLTWH(
        x, y, gameController.tileSize * 0.7, gameController.tileSize * 0.7);
  }

  void render(Canvas c) {
    Paint allyColor = Paint()..color = Colors.green;
    c.drawRect(allyRect, allyColor);
  }

  void update(double t) {
    if (!isDead) {
      double stepDistance = speed * t;
      Offset toPlayer =
          gameController.player.playerRect.center - allyRect.center;
      if (stepDistance <= toPlayer.distance - gameController.tileSize * 1.35) {
        Offset stepToPlayer =
            Offset.fromDirection(toPlayer.direction, stepDistance);
        allyRect = allyRect.shift(stepToPlayer);
      } else {
        isDead = true;
      }
    }
  }

  void onTapDown() {
    if (!isDead) {
      health--;
      if (health <= 0) {
        heal();
        isDead = true;
      }
    }
  }

  void heal() {
    if (!gameController.player.isDead &&
        gameController.player.currentHealth <
            gameController.player.maxHealth - support) {
      gameController.player.currentHealth += support;
    } else if (gameController.player.currentHealth <
            gameController.player.maxHealth &&
        gameController.player.currentHealth >=
            gameController.player.maxHealth - support) {
      gameController.player.currentHealth = gameController.player.maxHealth;
    }
    print(gameController.player.currentHealth);
  }
}
