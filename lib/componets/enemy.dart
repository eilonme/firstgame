import 'package:first_game/game_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Enemy {
  final GameController gameController;
  int health;
  int damage;
  double speed;
  Rect enemyRect;
  bool isDead = false;

  Enemy(this.gameController, double x, double y) {
    health = 5;
    damage = 250;
    speed = gameController.tileSize * 1.2;
    enemyRect = Rect.fromLTWH(
        x, y, gameController.tileSize * 1.2, gameController.tileSize * 1.2);
  }

  void render(Canvas c) {
    Color color;
    switch (health) {
      case 5:
        color = Colors.red[900];
        break;
      case 4:
        color = Colors.red[800];
        break;
      case 3:
        color = Colors.orange[900];
        break;
      case 2:
        color = Colors.orange[700];
        break;
      case 1:
        color = Colors.amber;
        break;
      default:
        color = Colors.black;
        break;
    }
    Paint enemyColor = Paint()..color = color;
    c.drawRect(enemyRect, enemyColor);
  }

  void update(double t) {
    if (!isDead) {
      double stepDistance = speed * t;
      Offset toPlayer =
          gameController.player.playerRect.center - enemyRect.center;
      print(toPlayer);
      if (stepDistance <= toPlayer.distance - gameController.tileSize * 1.35) {
        Offset stepToPlayer =
            Offset.fromDirection(toPlayer.direction, stepDistance);
        enemyRect = enemyRect.shift(stepToPlayer);
      } else {
        attack();
        isDead = true;
      }
    }
  }

  void attack() {
    if (!gameController.player.isDead) {
      gameController.player.currentHealth -= damage;
    }
    print(gameController.player.currentHealth);
  }

  void onTapDown() {
    if (!isDead) {
      health--;
      if (health <= 0) {
        isDead = true;
      }
    }
  }
}
