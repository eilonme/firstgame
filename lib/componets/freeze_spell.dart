import 'dart:ui' as ui;
import 'package:first_game/componets/enemy.dart';
import 'package:flutter/cupertino.dart';
import 'package:first_game/game_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:first_game/componets/small_enemy.dart';
import 'package:first_game/componets/ally.dart';

class FreezeSpell {
  GameController gameController;
  final int freezeInterval = 12000;
  int health;
  double speed;
  bool isUsed = false;
  Offset imageOffset;
  String imagePath;
  Paint paint;
  Rect freezeSpellRect;
  int now;
  int freezeStop;
  bool isDaed = false;

  FreezeSpell(this.gameController, double x, double y) {
    health = 1;
    speed = gameController.tileSize * 2.2;
    freezeSpellRect = Rect.fromLTWH(
        x, y, gameController.tileSize * 1.1, gameController.tileSize * 1.1);
  }

  /* How to use render()?
   * 
   * yourFunction() async {
   *   await render();
   * }
   * 
   */
  void render(Canvas c) {
    Color color = Colors.cyan[200];
    paint = Paint()..color = color;
    c.drawRect(freezeSpellRect, paint);
  }

  void update(double t) {
    if (!isDaed) {
      double stepDistance = speed * t;
      Offset toPlayer =
          gameController.player.playerRect.center - freezeSpellRect.center;
      print(toPlayer);
      if (stepDistance <= toPlayer.distance - gameController.tileSize * 1.35) {
        Offset stepToPlayer =
            Offset.fromDirection(toPlayer.direction, stepDistance);
        freezeSpellRect = freezeSpellRect.shift(stepToPlayer);
      } else {
        freeze();
      }
    }
  }

  void freeze() {
    if (!gameController.player.isDead) {
      now = DateTime.now().millisecondsSinceEpoch;
      freezeStop = now + freezeInterval;
      if (!isUsed) {
        if (now < freezeStop) {
          gameController.enemies.forEach((Enemy enemy) {
            enemy.speed = 0;
          });
          gameController.smallEnemies.forEach((SmallEnemy smallEnemy) {
            smallEnemy.speed = 0;
          });
          gameController.allies.forEach((Ally ally) {
            ally.speed = 0;
          });
        } else if (now == freezeStop) {
          isDaed = true;
        }
      }
    }
  } //
}

// Future<ui.Image> load(String asset) async {
//   ByteData data = await rootBundle.load(asset);
//   ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List());
//   ui.FrameInfo fi = await codec.getNextFrame();
//   return fi.image;
// }
