import 'package:first_game/game_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SmallEnemy {
  final GameController gameController;
  int health;
  int damage;
  double speed;
  Rect smallEnemyRect;
  bool isDead = false;

  SmallEnemy(this.gameController, double x, double y) {
    health = 3;
    damage = 50;
    speed = gameController.tileSize * 2;
    smallEnemyRect = Rect.fromLTWH(
        x, y, gameController.tileSize * 1, gameController.tileSize * 1);
  }

  void render(Canvas c) {
    Color color;
    switch (health) {
      case 3:
        color = Colors.purple[800];
        break;
      case 2:
        color = Colors.pink[500];
        break;
      case 1:
        color = Colors.pinkAccent[200];
        break;
      default:
        color = Colors.black;
        break;
    }
    Paint smallEnemyColor = Paint()..color = color;
    c.drawRect(smallEnemyRect, smallEnemyColor);
  }

  void update(double t) {
    if (!isDead) {
      double stepDistance = speed * t;
      Offset toPlayer =
          gameController.player.playerRect.center - smallEnemyRect.center;
      if (stepDistance <= toPlayer.distance - gameController.tileSize * 1.35) {
        Offset stepToPlayer =
            Offset.fromDirection(toPlayer.direction, stepDistance);
        smallEnemyRect = smallEnemyRect.shift(stepToPlayer);
      } else {
        attack();
        isDead = true;
      }
    }
  }

  void attack() {
    if (!gameController.player.isDead) {
      gameController.player.currentHealth -= damage;
    }
    print(gameController.player.currentHealth);
  }

  void onTapDown() {
    if (!isDead) {
      health--;
      if (health <= 0) {
        isDead = true;
      }
    }
  }
}
