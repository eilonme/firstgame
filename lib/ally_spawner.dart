import 'package:first_game/componets/ally.dart';
import 'package:first_game/game_controller.dart';
import 'package:flutter/cupertino.dart';

class AllySpawner {
  final GameController gameController;
  final int initialSpawnInterval = 40000;
  final int maxSpawnInterval = 12000;
  final int minSpawnInterval = 6000;
  final int intervalChange = 3;
  final int maxAllies = 1;
  int currentInterval;
  int nextSpawn;

  AllySpawner(this.gameController) {
    killAllAllies();
    initialize();
  }

  void initialize() {
    currentInterval = maxSpawnInterval;
    nextSpawn = DateTime.now().millisecondsSinceEpoch + initialSpawnInterval;
  }

  void killAllAllies() {
    gameController.allies.forEach((Ally ally) => ally.isDead = true);
  }

  void update(double t) {
    int now = DateTime.now().millisecondsSinceEpoch;
    if (gameController.allies.length <= maxAllies && now > nextSpawn) {
      gameController.spawnAllies();
      if (currentInterval > minSpawnInterval) {
        currentInterval -= intervalChange;
        currentInterval -= (currentInterval * 0.1).toInt();
      }

      nextSpawn = now + currentInterval;
    }
  }
}
